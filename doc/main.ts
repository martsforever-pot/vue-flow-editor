import Vue from 'vue'
import App from './App.vue'
// import App from './test/test-command.vue'

import VueFlowEditor from '../src/index'
import vca from '@vue/composition-api'

Vue.use(vca)
Vue.use(VueFlowEditor)
// @ts-ignore
Vue.use(window.ELEMENT)
Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')
