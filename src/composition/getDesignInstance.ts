import {getCurrentInstance} from "@vue/composition-api";

export function getDesignInstance() {
    const ctx = getCurrentInstance()
    if (!ctx) {return ctx}
    return ctx.proxy || ctx
}
